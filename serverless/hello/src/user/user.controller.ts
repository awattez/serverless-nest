import { Controller, Get } from '@nestjs/common'
import { UserEntity, UserService } from '@serverless-nest/entities'

@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  getHello(): UserEntity {
    return this.userService.getHelloUser()
  }
}
