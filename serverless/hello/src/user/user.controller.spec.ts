import { Test, TestingModule } from '@nestjs/testing'
import { UserController } from './user.controller'
import { UserService } from '@serverless-nest/entities'

describe('AppController', () => {
  let app: TestingModule

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService],
    }).compile()
  })

  describe('getHello', () => {
    it('should return an Hello World User', () => {
      const appController = app.get<UserController>(UserController)
      expect(appController.getHello()).toEqual({ id: 'id', email: 'hello@world.com' })
    })
  })
})
