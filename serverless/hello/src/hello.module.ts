import { Module } from '@nestjs/common'
import { EntityModule } from '@serverless-nest/entities'
import { UserController } from './user'

@Module({
  imports: [EntityModule],
  controllers: [UserController],
  providers: [],
})
export class HelloModule {}
