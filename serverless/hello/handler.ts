import { AWSServerlessFactory } from '@serverless-nest/aws-serverless'
import { HelloModule } from './src'

const { binaryMimeTypes } = process.env

export const handler = AWSServerlessFactory.createHandler(HelloModule, {
  binaryMimeTypes: binaryMimeTypes && binaryMimeTypes.split(','),
})
