import { ArgumentsHost, Catch, ExceptionFilter, HttpException, Logger } from '@nestjs/common'

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter<HttpException> {
  readonly logger: Logger = new Logger(HttpExceptionFilter.name)

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp()
    const request = ctx.getRequest()
    const response = ctx.getResponse()
    this.logger.debug(exception)
    // only for http context, not graphql
    if (request && response) {
      const statusCode = exception.getStatus()

      const error: Record<string, any> = {
        path: request && request.url,
        timestamp: new Date().getTime(),
        message: exception.message,
      }

      if (process.env.NODE_ENV !== 'production') {
        error.stacktrace = exception.stack
        error.error = exception.getResponse()
      }

      response.status(statusCode).json(error)
    }
  }
}
