import { ValidationPipe } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { ExpressAdapter } from '@nestjs/platform-express'
import { Handler } from 'aws-lambda'
import serverless from 'aws-serverless-express'
import awsServerlessExpressMiddleware from 'aws-serverless-express/middleware'
import 'class-transformer'
import 'class-validator'
import express from 'express'
import { Server } from 'http'
import 'reflect-metadata'
import 'rxjs'
import { HttpExceptionFilter } from './http-exception.filter'

/**
 * AWS Serverless handler factory options.
 */
export interface AWSServerlessFactoryOptions {
  prefix?: string
  binaryMimeTypes?: string[]
}

/**
 * AWS Serverless handler factory.
 */
export class AWSServerlessFactory {
  /**
   * Cached express server.
   */
  static server: Server

  /**
   * Creates an AWS Lambda function handler for a Nest application.
   *
   * @param module Nest module that will handle the AWS Serverless event.
   * @param [options] Serverless configuration.
   */
  static createHandler(module: any, options: AWSServerlessFactoryOptions = {}): Handler {
    return async (event, context) => {
      // instantiate microservice
      if (!AWSServerlessFactory.server) {
        const expressApp = express()
        expressApp.disable('x-powered-by')

        const adapter = new ExpressAdapter(expressApp)
        const app = await NestFactory.create(module, adapter)

        if (options.prefix) {
          app.setGlobalPrefix(options.prefix)
        }
        app.use(awsServerlessExpressMiddleware.eventContext())
        app.useGlobalFilters(new HttpExceptionFilter())
        app.useGlobalPipes(new ValidationPipe())

        await app.init()

        AWSServerlessFactory.server = serverless.createServer(expressApp, null, options.binaryMimeTypes)
      }

      return serverless.proxy(AWSServerlessFactory.server, event, context, 'PROMISE').promise
    }
  }

  /**
   * Clears cached server.
   */
  static clearCache() {
    if (AWSServerlessFactory.server) {
      AWSServerlessFactory.server.close()
      delete AWSServerlessFactory.server
    }
  }
}
