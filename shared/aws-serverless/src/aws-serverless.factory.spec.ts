process.env.binaryMimeTypes = JSON.stringify([])

import { Controller, Get, Module } from '@nestjs/common'
import { AWSServerlessFactory } from './aws-serverless.factory'

// tslint:disable:max-classes-per-file

@Controller('test')
class AWSServerlessExpressController {
  @Get()
  async test() {
    return 'some test value'
  }
}

@Module({
  controllers: [AWSServerlessExpressController],
})
class AWSLambdaExpressModule {}

describe('AWSServerlessFactory', () => {
  it('should create handler function', async () => {
    const handler = AWSServerlessFactory.createHandler(AWSLambdaExpressModule)

    expect(handler).toBeInstanceOf(Function)

    AWSServerlessFactory.clearCache()
  })

  it('should properly return a result', async () => {
    const handler = AWSServerlessFactory.createHandler(AWSLambdaExpressModule)
    const result = await handler({ method: 'GET', path: '/test' }, null, null)

    expect(result).toMatchObject({
      statusCode: 200,
      body: 'some test value',
    })
  })

  it('should use cached server on further calls', async () => {
    expect(AWSServerlessFactory.server).toBeTruthy()

    const handler = AWSServerlessFactory.createHandler(AWSLambdaExpressModule)
    const result = await handler({ method: 'GET', path: '/test' }, null, null)

    expect(result).toMatchObject({
      statusCode: 200,
      body: 'some test value',
    })

    AWSServerlessFactory.clearCache()
  })
})
