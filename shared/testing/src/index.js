const e2eTestsConfig = require('./jest-e2e.config')
const unitTestsConfig = require('./jest-unit.config')

module.exports = {
  e2eTestsConfig,
  unitTestsConfig,
}
