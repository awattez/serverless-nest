/**
 * Shared jest configuration.
 */
module.exports = {
  coverageDirectory: './.coverage',
  moduleFileExtensions: ['js', 'json', 'ts'],
  testEnvironment: 'node',
  testRegex: '\\.spec\\.ts$',
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
  coverageThreshold: {
    global: {
      statements: 70,
    },
  },
}
