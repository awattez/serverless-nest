const baseConfig = require('./jest-unit.config')

/**
 * Shared jest e2e configuration.
 */
module.exports = {
  ...baseConfig,
  testRegex: '\\.e2e-spec\\.ts$',
}
