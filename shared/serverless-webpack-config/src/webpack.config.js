const { resolve } = require('path')
const slsw = require('serverless-webpack')
const nodeExternals = require('webpack-node-externals')
const { findNodeModules } = require('./utils')

// must start from monorepo root folder
const nodeModules = findNodeModules(resolve(__dirname, '..', '..', '..'))

module.exports = {
  mode: slsw.lib.webpack.isLocal ? 'development' : 'production',
  entry: slsw.lib.entries,
  devtool: 'source-map',
  optimization: {
    minimize: false,
  },
  externals: nodeModules.map(nodeModule =>
    nodeExternals({
      modulesDir: nodeModule,
      whitelist: /^@serverless-nest\/(.*)/,
    }),
  ),
  resolve: {
    extensions: ['.js', '.json', '.ts'],
  },
  stats: slsw.lib.webpack.isLocal ? 'minimal' : 'normal',
  target: 'node',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /\.spec\.tsx?$/,
      },
    ],
  },
}
