const { readdirSync } = require('fs')
const { join } = require('path')

/**
 * Recursively finds node_modules within source.
 *
 * @param source Source path.
 * @returns {*} All node_modules directories withing source.
 */
function findNodeModules(source) {
  // list all directories within source
  const dirs = readdirSync(source, {
    withFileTypes: true,
  }).filter(file => file.isDirectory())

  return (
    []
      // find node_modules within source
      .concat(...dirs.filter(dir => dir.name === 'node_modules').map(dir => join(source, dir.name)))
      // recursively find node_modules within source children
      .concat(
        ...dirs
          .filter(dir => !dir.name.startsWith('.'))
          .filter(dir => dir.name !== 'node_modules')
          .map(dir => findNodeModules(join(source, dir.name))),
      )
  )
}

module.exports = {
  findNodeModules,
}
