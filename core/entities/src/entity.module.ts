import { Module } from '@nestjs/common'
import { UserService } from './users'

@Module({
  imports: [],
  exports: [UserService],
  providers: [UserService],
})
export class EntityModule {}
