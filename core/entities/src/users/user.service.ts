import { Injectable } from '@nestjs/common'
import { UserEntity } from '@serverless-nest/entities'

@Injectable()
export class UserService {
  getHelloUser(): UserEntity {
    return { id: 'id', email: 'hello@world.com' }
  }
}
